

exports = module.exports = function (app, router){

	app.use("/"+require("./Config.js").service_name, router);
	
	router.post('/download', require('./app/logics/download.js'));

	app.get('*', function(req, res) {
	    
  		 res.send('Link error');
	});
}
