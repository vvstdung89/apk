var Common = require('../libs/Common.js');
var APKInfoModel = require('../../models/APKInfoModel.js');

exports  = module.exports  = function(req,res){
	var locals = res.locals;
	var redis_client = locals.redis_client;
	var result = {};
	var id = req.body.id;
	
	
	APKInfoModel.findOne({
		_id: id
	})
	.exec(function(error, find_res){
		if (error){
			result.status = "004";
			result.data = "Database Connection error";
			res.json(result)
			console.log("Get error: " + error)
		} else {
			if (find_res) {
				result.status = "000";
				result.data = find_res;
				res.json(result)
				console.log("Get success: " + result)
			} else {
				result.status = "001";
				result.data = "No video file found";
				res.json(result)
				console.log("Get not found - id " + id)
			}
		}
	})
		
}