var Aria2 = require('aria2');

function Aria2_Client() {
	this.option = {
		host: 'localhost',
		port: 52139,
		secure: false,
		secret: 'heocon151189'
	}
	this.aria2c_client = new Aria2(this.option);
	this.aria2c_client.open();
}


//support only one download for one conenction // if there are many, we should check the on Complete function confliction
Aria2_Client.prototype.download = function(info, callback) {
	
	var filepath = info.filepath

	var drivename = info.drivename
	var url = info.url
	var self = this;
	this.aria2c_client.addUri([url],{"out":drivename, "dir":require('path').dirname(filepath), "max-connection-per-server":"10" , "split":"10"}, function(err,res_aria){
		console.log(err || res_aria)
		// if (!err){
		// 	self.aria2c_client.pause(res_aria, function(a,b){
		// 		console.log(a||b)
		// 		self.aria2c_client.unpause(res_aria, function(a,b){
		// 			console.log(a||b)
		// 		})
		// 	})
		// }
		callback(err,res_aria)
  	});
}


Aria2_Client.prototype.onComplete = function(callback) {
	this.aria2c_client.onDownloadComplete = function(guid){
		try {
			var gid = JSON.parse(JSON.stringify(guid)).gid
			callback(null, gid);
		} catch(err){
			callback(err);
		}
	}
}

Aria2_Client.prototype.onError = function(callback) {
	this.aria2c_client.onDownloadError = function(result){
		console.log("Download error ")
		callback(result.gid)
	}
}


exports = module.exports = Aria2_Client;