var exec = require('child_process').exec
var execSync = require('child_process').execSync;


function repeatGetSource(url, callback){

	function iterate(index, callback){
		if (index > 10) {
			callback("");
			return;
		}
		exec("curl "+ url +" -s | grep \"sources:\"", function(e, stdout, stderr){
			if (!e){
				var fid = stdout.indexOf("[");
				var eid = stdout.indexOf("]");
				callback(stdout.substring(fid,eid+1));
			} else {
				iterate(++index,callback);
			}
		})
	}
	iterate(0,callback);
}


exports = module.exports = {
	getSource : function(url, callback) {
		console.log(" ---- get stream from " + url);
		repeatGetSource(url, function(src){
			src = src.replace(/file/g,'"file"');
			src = src.replace(/label/g,'"label"');
			src = JSON.parse(src);
			var max = src[src.length-1];
			console.log(src);
			if (Number(max.label.replace("p","")) >= 360){
				var stream = {
					link : max.file,
					res : max.label
				}
				callback(stream);
			} else {
				console.log("not enough resolution " + Number(max.label.replace("p","")))
				callback("");
			}
		})
	}
}