var request = require('request');
var service_name = "DatabaseService";

exports = module.exports = {

	create: function(info, callback){
		var caller = "create";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/apkinfo/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},

	remove: function(info, callback){
		var caller = "remove";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/apkinfo/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},


	get: function(info, callback){
		var caller = "get";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/apkinfo/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},


	list: function(info, callback){
		var caller = "list";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/apkinfo/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
		        } else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},


	update: function(info, callback){
		var caller = "update";
		var service_location = "http://" + require('../Config.js').getService(service_name)[0]+"/"+service_name;
		request.post(
		    service_location + '/apkinfo/' + caller,
		    { form: info },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            callback(null, body);
				} else if (error){
		        	callback(error, body);
		        } else {
					callback(true, "Reply status code: " + response.statusCode);
		        }
		    }
		);
	},
	
};
