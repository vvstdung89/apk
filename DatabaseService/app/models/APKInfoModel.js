var mongoose     = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');


var Schema       = mongoose.Schema;

var APKInfoSchema   = new Schema({
	name: {type: String, index: true, required: true},
	nameID: {type: String, index: true, required: true, unique: true},
	originalLink: {type:String, required: true},
	storeLink: {type: String},
	rateTotal: {type: Number},
	rateAverage: {type: Number,index: true},
	category: {type: String, index: true},
	author: {type: String, index: true},
	publishDate: {type: Number, index: true},
	info: String,
	poster: [String],
	status: {type: String, index: true, default:"pending"},
	xapk: {type: Boolean, default:false}
});

APKInfoSchema.set('autoIndex', true);

APKInfoSchema.plugin(autoIncrement.plugin, 'APKInfo');

exports = module.exports = mongoose.model('APKInfo', APKInfoSchema);