# This script is used to deploy cide into server
# Note: you should add your ssh key to all the server, so that you dont need to type password everytime you run the code
# 		- ssh-copy-id user@ip
# Author: Van Quoc Dung

###### Modified section ######
# All server IP
server1="188.166.229.247"

# All designated port
databaseservice_port="10011";
download_port="10012";

# Get source folder from parameter
srcFolder=${1%/}

# All project name usage => add server and port use
if [[ "$srcFolder" == DatabaseService ]] ;then
	server=( "$server1" )
	port=$databaseservice_port
	# All project name usage => add server and port use
elif [[ "$srcFolder" == DownloadService ]] ;then
	server=( "$server1" )
	port=$download_port

else
	echo "Please register project folder or specify in command line "
	exit;
fi

###### End modified section ######
# Only modify below code if you know what to dop
task=$2
mode="production"
desFolder="/home/live"

init(){
	copy ${server[@]}
	echo "Init Project ...."
	for i in ${server[@]}
	do
		ssh live@$i "cd ${desFolder}/$srcFolder && npm install"
	done
}

copy(){
	echo "Copy Files ...."
	for i in ${server[@]}
	do
		rsync -avz --exclude $srcFolder/node_modules $srcFolder live@$i:$desFolder
	done
	
}

restart(){	
	echo "Restart service ...."
	for i in ${server[@]}
	do
		ssh live@$i "cd ${desFolder}/$srcFolder && NODE_ENV=${mode} ./run.sh ${port}"
	done
	
}

log(){
	echo "Reading Log File ...."
	for i in ${server[@]}
	do
		ssh live@$i tail -f -n 100 ${desFolder}/$srcFolder/main.log
	done
}

if [ "$task" = "init" ] ;then
	init 
	restart 
elif [ "$task" = "copy" ] ;then
	copy 
	restart 
	log
elif [ "$task" = "restart" ] ;then
	restart 
elif [ "$task" = "log" ] ;then
	log 
else
	echo "[ERROR] Usage: ./sync Project [init |copy|restart|log]"
fi

