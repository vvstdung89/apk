

exports = module.exports = function (app, router){

	app.use("/"+require("./Config.js").service_name, router);
	
	var apkinfo = {
		create : './app/logics/apkinfo/create.js',
		list: './app/logics/apkinfo/list.js',
		delete: './app/logics/apkinfo/delete.js',
		update : './app/logics/apkinfo/update.js',
		get: './app/logics/apkinfo/get.js',
	}

	var apkfile = {
		create : './app/logics/apkfile/create.js',
		list: './app/logics/apkfile/list.js',
		delete: './app/logics/apkfile/delete.js',
		update : './app/logics/apkfile/update.js',
		get: './app/logics/apkfile/get.js',
	}


	router.post('/apkinfo/create', require(apkinfo.create));
	router.post('/apkinfo/list', require(apkinfo.list));
	router.post('/apkinfo/delete', require(apkinfo.delete));
	router.post('/apkinfo/update', require(apkinfo.update));
	router.post('/apkinfo/get', require(apkinfo.get));

	router.post('/apkfile/create', require(apkfile.create));
	router.post('/apkfile/list', require(apkfile.list));
	router.post('/apkfile/delete', require(apkfile.delete));
	router.post('/apkfile/update', require(apkfile.update));
	router.post('/apkfile/get', require(apkfile.get));
	
}
