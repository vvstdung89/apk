var APKInfoModel = require('../../models/APKInfoModel.js');
var Common = require('../libs/Common.js');


exports = module.exports = function(req,res){
	var position = req.body.pos;

	var redis_client = res.locals.redis_client;
	var range = req.body.range;

	var result = {};
	
	//check input	
	if ( !position || !range  ){
		result.status = "error";
		result.data = "Position, range must be specified.";
		res.json(result);
		return;
	} 

	APKInfoModel.find()
	.count()
	.exec(function(count_error, count){
		if (!count_error){
			APKInfoModel.find()
			.skip(position)
			.limit(range)
			.exec(function(list_error, objs){
				if (list_error){
					result.status = "004";
					result.data = "Database connection error";
					res.json(result);
					console.log("List error ");
					console.log(list_error);
					return;
				} else {
					console.log("no redis")
					result.status = "000";
					result.total = count;
					result.data = objs;
					res.json(result);
					console.log("List success ");
				}
			})
		} else {
			result.status = "004";
			result.data = "Database connection error";
			res.json(result);
			console.log("Count error ");
			console.log(count_error);
		}
	})

	
		
	

	



	

}
