var APKFileModel = require('../../models/APKFileModel.js');
var Common = require('../libs/Common.js');
exports = module.exports = function(req, res){
	var apk = req.body;

	var result = {};
	//check input	
	if ( !apk.id ){
		result.status = "error";
		result.data = "Missing [id]";
		res.json(result)
		console.log("Update error:  missing id")
		return;
	} 

	//check apk exist
	APKFileModel.findOne(
		{ _id: apk.id },
		function(cexist_err, obj){
			if (cexist_err==null  && !obj) { //if not exit
				result.status = "001";
				result.data = "apk is not exist";
				res.json(result)
				console.log("Update error:  " + JSON.parse(result))
			} else if (cexist_err==null){ 
				console.log(obj)
				APKModel.update({
					_id: apk.id
				},
				apk,function (err, updateFilm) {
					if (err){
						result.status = "004";
						result.data = "Database Connection Error";
						res.json(result)
						console.log("Update error - update:  " + err)
						return;
					}
					result.status = "000";
					result.data = apk;
					res.json(result)
					console.log("Update success:  " + JSON.parse(apk))
				});
			} else {
				result.status = "004";
				result.data = "Cannot access database";
				callback(result)
				console.log("Update error - check exist:  " + cexist_err)
			}
		}	
	)
}