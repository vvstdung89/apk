var mongoose     = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');

var Schema       = mongoose.Schema;

var APKFileSchema   = new Schema({
	appID: {type: Number, index: true, required: true},
	filePath: {type: String, require: true},
	version: {type: String},
	versionID: {type: Number},
	downloadTime: {type: Number, index: true, required: true},
	updateTime: {type: Number, index: true},
	signature: {type: String},
});

APKFileSchema.plugin(autoIncrement.plugin, 'APKFile');

exports = module.exports = mongoose.model('APKFile', APKFileSchema);