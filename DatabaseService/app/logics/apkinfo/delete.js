var Common = require('../libs/Common.js');
var APKInfoModel = require('../../models/APKInfoModel.js');

exports  = module.exports  = function(req,res){
	var locals = res.locals;
	var result = {};
	var id = req.body.id;
	var redis_client = locals.redis_client;

	APKInfoModel.remove({
		_id: id
	}, function (delete_error){
		if (delete_error){
			result.status = "004";
			result.data = "Database Connection error";
			res.json(result);
			console.log("Delete error: " + delete_error);
			return;
		} else {
			result.status = "000";
			result.data = "Delete apk successfully";
			res.json(result);
			console.log("Delete success: " + JSON.parse(result));
			return;
		}
	})

}